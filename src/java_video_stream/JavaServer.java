package java_video_stream;

import com.sun.jna.Native;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import javax.imageio.ImageIO;

import javax.swing.*;

import com.sun.jna.NativeLibrary;
import com.sun.jna.platform.win32.WinUser.POINT;

import java.nio.file.Files;
import uk.co.caprica.vlcj.binding.LibVlc;

import uk.co.caprica.vlcj.player.MediaPlayerFactory;
import uk.co.caprica.vlcj.player.embedded.EmbeddedMediaPlayer;
import uk.co.caprica.vlcj.player.embedded.videosurface.CanvasVideoSurface;
import uk.co.caprica.vlcj.runtime.RuntimeUtil;
import uk.co.caprica.vlcj.runtime.x.LibXUtil;
//import uk.co.caprica.vlcj.runtime.windows.WindowsRuntimeUtil;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;

/**
 *
 * @author Milan
 */
public class JavaServer {

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static InetAddress[] inet;
	public static int[] port;
	public static int i;
	static int count = 0;
	public static BufferedReader[] inFromClient;
	public static DataOutputStream[] outToClient;
	
	public static void main(String[] args) throws Exception
	{
		JavaServer jv = new JavaServer();
	}

	
	
	public JavaServer() throws Exception {
		
		
		NativeLibrary.addSearchPath("libvlc", "c:\\Program Files\\VideoLAN\\VLC");

		JavaServer.inet = new InetAddress[30];
		port = new int[30];


		ServerSocket welcomeSocket = new ServerSocket(6782);//bilo koji broj od 5000 pa navise koji se ne koristi
		System.out.println(welcomeSocket.isClosed());
		Socket connectionSocket[] = new Socket[30];
		inFromClient = new BufferedReader[30];
		outToClient = new DataOutputStream[30];

		DatagramSocket serv = new DatagramSocket(4321);

		byte[] buf = new byte[62000];
		// Socket[] sc = new Socket[5];
		DatagramPacket dp = new DatagramPacket(buf, buf.length);

		Canvas_Demo canv = new Canvas_Demo();
		System.out.println("Gotcha");

		// OutputStream[] os = new OutputStream[5];

		i = 0;
		
		
		

		while (true) {

			System.out.println(serv.getPort());
			serv.receive(dp);
			System.out.println(new String(dp.getData()));
			buf = "starts".getBytes();

			inet[i] = dp.getAddress();
			port[i] = dp.getPort();

			DatagramPacket dsend = new DatagramPacket(buf, buf.length, inet[i], port[i]);
			serv.send(dsend);

			Vidthread sendvid = new Vidthread(serv);

			System.out.println("waiting\n ");
			connectionSocket[i] = welcomeSocket.accept();
			System.out.println("connected " + i);

			inFromClient[i] = new BufferedReader(new InputStreamReader(connectionSocket[i].getInputStream()));
			outToClient[i] = new DataOutputStream(connectionSocket[i].getOutputStream());
			outToClient[i].writeBytes("Connected: from Server\n");

			
			

			System.out.println(inet[i]);
			sendvid.start();

			i++;

			if (i == 30) {
				break;
			}
		}
	}
}

class Vidthread extends Thread {//print

	int clientno;
	// InetAddress iadd = InetAddress.getLocalHost();
	JFrame jf = new JFrame("SC pa slanje");
	JLabel jleb = new JLabel();

	DatagramSocket soc;

	Robot rb = new Robot();
	// Toolkit tk = Toolkit.getDefaultToolkit();

	// int x = tk.getScreenSize().height;
	// int y = tk.getScreenSize().width;

	byte[] outbuff = new byte[62000];

	BufferedImage mybuf;
	ImageIcon img;
	Rectangle rc;
	
	int bord = Canvas_Demo.panel.getY() - Canvas_Demo.frame.getY();
	// Rectangle rc = new Rectangle(new
	// Point(Canvas_Demo.frame.getX(),Canvas_Demo.frame.getY()),new
	// Dimension(Canvas_Demo.frame.getWidth(),Canvas_Demo.frame.getHeight()));

	// Rectangle rv = new Rectangle(d);
	public Vidthread(DatagramSocket ds) throws Exception {
		soc = ds;

		System.out.println(soc.getPort());
		jf.setSize(500, 400);
		jf.setLocation(500, 400);
		jf.setVisible(true);
	}

	public void run() {
		while (true) {
			try {

				int num = JavaServer.i;

				rc = new Rectangle(new Point(Canvas_Demo.frame.getX() + 8, Canvas_Demo.frame.getY() + 27),
						new Dimension(Canvas_Demo.panel.getWidth(), Canvas_Demo.frame.getHeight() / 2));

				// System.out.println("another frame sent ");

				mybuf = rb.createScreenCapture(rc);//uzimaje screen shota

				img = new ImageIcon(mybuf);

				jleb.setIcon(img);
				jf.add(jleb);
				jf.repaint();
				jf.revalidate();
				// jf.setVisible(true);
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				
				ImageIO.write(mybuf, "jpg", baos);//zapis sc kao jpg
				
				outbuff = baos.toByteArray();

				for (int j = 0; j < num; j++) {
					DatagramPacket dp = new DatagramPacket(outbuff, outbuff.length, JavaServer.inet[j],
							JavaServer.port[j]);
					//System.out.println("Frame Sent to: " + JavaServer.inet[j] + " port: " + JavaServer.port[j]
						//	+ " size: " + baos.toByteArray().length);
					soc.send(dp);
					baos.flush();
				}
				Thread.sleep(15);

				// baos.flush();
				// byte[] buffer = baos.toByteArray();
			} catch (Exception e) {

			}
		}

	}

}

class Canvas_Demo {

	// Create a media player factory
	private MediaPlayerFactory mediaPlayerFactory;

	// Create a new media player instance for the run-time platform
	private EmbeddedMediaPlayer mediaPlayer;

	public static JPanel panel;
	public static JPanel myjp;
	private Canvas canvas;
	public static JFrame frame;
	public static JTextArea ta;
	public static JTextArea txinp;
	public static int xpos = 0, ypos = 0;
	String url = "";

	// Konstruktor
	public Canvas_Demo() {

		// Panel sa vlc playerom
		panel = new JPanel();
		panel.setLayout(new BorderLayout());

		JPanel mypanel = new JPanel();
		mypanel.setLayout(new GridLayout(2, 1));

		
		canvas = new Canvas();
		canvas.setBackground(Color.BLACK);
		// canvas.setSize(640, 480);
		panel.add(canvas, BorderLayout.CENTER);
		// panel.revalidate();
		// panel.repaint();

		
		mediaPlayerFactory = new MediaPlayerFactory();
		mediaPlayer = mediaPlayerFactory.newEmbeddedMediaPlayer();
		CanvasVideoSurface videoSurface = mediaPlayerFactory.newVideoSurface(canvas);
		mediaPlayer.setVideoSurface(videoSurface);

		
		frame = new JFrame("VideoStreamServer");
		// frame.setLayout(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocation(200, 0);
		frame.setSize(640, 960);
		frame.setAlwaysOnTop(true);

		// panel.setSize(940, 480);
		// mypanel.setLayout(new BorderLayout());
		mypanel.add(panel);
		frame.add(mypanel);
		frame.setVisible(true);
		xpos = frame.getX();
		ypos = frame.getY();

		//Dizajn playera

		myjp = new JPanel(new GridLayout(4, 1));

		Button bn = new Button("Izaberi fajl");
                Button v1 = new Button("V1");
                Button v2 = new Button("V3");
                Button v3 = new Button("V3");
		myjp.add(bn);
                myjp.add(v1);
                myjp.add(v2);
                myjp.add(v3);

		mypanel.add(myjp);
		mypanel.revalidate();
		mypanel.repaint();


		bn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				JFileChooser jf = new JFileChooser();
				jf.showOpenDialog(frame);
				File f;
				f = jf.getSelectedFile();
				url = f.getPath();
				System.out.println(url);

				mediaPlayer.playMedia(url);
			}
		});
                
                v1.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
                            String url = "C:\\Users\\Milan\\Desktop\\Serbia - beauty in nature.MP4";
                            System.out.println(url);

				mediaPlayer.playMedia(url);
			}
		});
                
                v2.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
                            String url = "C:\\Users\\Milan\\Desktop\\Serbia - beauty in nature.MP4";
                            System.out.println(url);

				mediaPlayer.playMedia(url);
			}
		});

                v3.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
                            String url = "C:\\Users\\Milan\\Desktop\\Serbia - beauty in nature.MP4";
                            System.out.println(url);

				mediaPlayer.playMedia(url);
			}
		});                
                

	}
}




